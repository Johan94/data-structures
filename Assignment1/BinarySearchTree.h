// BinarySearchTree.h

#pragma once

#include "stdafx.h"
#include "LinkedList.h"

using namespace std;

template <class T>
class BinarySearchTree
{
private:
	struct Node
	{
		Node* left;
		Node* right;
		T value;
	};

	Node* top;

public:
	BinarySearchTree()
	{
		top = nullptr;
	}

	~BinarySearchTree()
	{
		Clear();
	}

	void Insert(T value)
	{
		AddNodeAt(top, value);
	}

	Node* CreateNode(T value)
	{
		Node* newNode = new Node;
		newNode->value = value;
		newNode->left = nullptr;
		newNode->right = nullptr;

		return newNode;
	}

	void Clear()
	{
		DeleteSubtree(top);
		top = nullptr;
	}

	bool Find(T value)
	{
		Node* node = GetNode(value);
		return (node != nullptr);
	}

	int Size()
	{
		return SubtreeSize(top);
	}

	LinkedList<T>* PreOrder()
	{
		LinkedList<T>* list = new LinkedList<T>;
		PreOrderRec(top, list);
		return list;
	}

	LinkedList<T>* InOrder()
	{
		LinkedList<T>* list = new LinkedList<T>;
		InOrderRec(top, list);
		return list;
	}

	LinkedList<T>* PostOrder()
	{
		LinkedList<T>* list = new LinkedList<T>;
		PostOrderRec(top, list);
		return list;
	}

private:
	void AddNodeAt(Node* at, T value)
	{
		if (top == nullptr)
		{
			top = CreateNode(value);
		}
		else if (value > at->value)
		{
			if (at->right != nullptr)
				AddNodeAt(at->right, value);
			else
				at->right = CreateNode(value);
		}
		else if (value < at->value)
		{
			if (at->left != nullptr)
				AddNodeAt(at->left, value);
			else
				at->left = CreateNode(value);
		}
		else
			return; // lazily do nuthin' if the value is already in the tree
	}

	void InOrderRec(Node* at, LinkedList<T>* list) // the list parameter is a pointer to the LinkedList instance we push the values to
	{
		if (top == nullptr)
			return;

		if (at->left)
			InOrderRec(at->left, list); // recursively call the function again, starting at the left subnode

		list->PushBack(at->value); // store the value in the list

		if (at->right)
			InOrderRec(at->right, list);
	}

	void PreOrderRec(Node* at, LinkedList<T>* list)
	{
		if (top == nullptr)
			return;

		list->PushBack(at->value);

		if (at->left)
			PreOrderRec(at->left, list);

		if (at->right)
			PreOrderRec(at->right, list);
	}

	void PostOrderRec(Node* at, LinkedList<T>* list)
	{
		if (top == nullptr)
			return;

		if (at->left != nullptr)
			PostOrderRec(at->left, list);

		if (at->right != nullptr)
			PostOrderRec(at->right, list);

		list->PushBack(at->value); // store the value in the list
	}

	Node* GetNode(T value)
	{
		return GetNodeRec(top, value);
	}

	Node* GetNodeRec(Node* at, T value)
	{
		if (at == nullptr)
			return nullptr;
		
		if (at->value == value)
			return at;
		else
		{
			if (value < at->value)
				return GetNodeRec(at->left, value);
			else
				return GetNodeRec(at->right, value);
		}
	}

	void DeleteSubtree(Node* at)
	{
		if (at != nullptr)
		{
			if (at->left != nullptr)
				DeleteSubtree(at->left);
			if (at->right != nullptr)
				DeleteSubtree(at->right);
			delete at;
		}
	}

	int SubtreeSize(Node* at)
	{
		int size = 0;
		if (at != nullptr)
		{
			if (at->left != nullptr)
				size += SubtreeSize(at->left);
			if (at->right != nullptr)
				size += SubtreeSize(at->right);
			size++;
		}
		return size;
	}
};