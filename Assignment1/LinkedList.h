// LinkedList.h

#pragma once

#include "stdafx.h"

using namespace std;

template <class T>
class LinkedList
{
	struct Node
	{
		Node* child;
		T value;
	};

public:
	LinkedList()
	{
		root = nullptr;
	};

	~LinkedList()
	{
		Clear();
	};

	void PushFront(T value)
	{
		//cout << "PushFront(" << value << ")\n";
		if (root == nullptr)
		{
			root = new Node;
			root->child = nullptr;
			root->value = value;
		}
		else
		{
			Node* newNode = root;
			root = new Node;
			root->child = newNode;
			root->value = value;
		}
	};

	void PushBack(T value)
	{
		//cout << "PushBack(" << value << ")\n";
		Node* newNode = new Node;
		newNode->value = value;
		newNode->child = nullptr;
		if (root == nullptr)
		{
			root = newNode;
			return;
		}
		Node* lastnode = FindLastNode();
		lastnode->child = newNode;
	};

	void PopFront()
	{
		//cout << "PopFront()\n";
		if (Size() > 1)
		{
			Node* temp = AtIndex(1);
			delete root;
			root = temp;
		}
		else
			delete root;
	};

	void PopBack()
	{
		//cout << "PopBack()\n";
		if (Size() == 0)
			return;
		else if (Size() == 1)
		{
			delete root;
			return;
		}

		Node* penultimate = AtIndex(Size() - 2);
		Node* last = AtIndex(Size() - 1);
		delete last;
		penultimate->child = nullptr;
	};

	void Clear()
	{
		//cout << "Clear()\n";
		int size = Size();
		for (int i = (size - 1); i >= 1; i--)
		{
			Node* tempNode = AtIndex(i - 1);
			delete tempNode->child;
			tempNode->child = nullptr;
		}
		if (root)
		{
			root->child = nullptr;
			delete root;
			root = nullptr;
		}
	};

	int Find(T value)
	{
		Node* itr = root;

		if (itr == nullptr)
			return -1;
		if (itr->value == value)
			return 0;

		int index = 0;
		while (itr != nullptr)
		{
			if (itr->value == value)
				return index;

			if (itr->child != nullptr)
			{
				itr = itr->child;
				index++;
			}
			else
				return -1;

		}
		return -1;
	};

	int Size()
	{
		if (root == nullptr)
		{
			return 0;
		}

		int count = 1;
		Node* itr = root;

		while (itr)
		{
			if (itr->child == nullptr)
			{
				return count;
			}
			itr = itr->child;
			count++;
		}

		return count;
	};

	Node* AtIndex(int index)
	{
		if (index + 1 > Size() || Size() < 1)
		{
			return nullptr;
		}

		Node* itr = root;
		for (int i = 0; i < index; i++)
		{
			itr = itr->child;
		}

		return itr;
	};

	void Display()
	{
		for (int i = 0; i < Size(); i++)
		{
			cout << "index[" << i << "] = " << AtIndex(i)->value << "\n";
		}
	}

private:
	Node* root;

	Node* FindLastNode()
	{
		return (AtIndex(Size() - 1));
	}

};