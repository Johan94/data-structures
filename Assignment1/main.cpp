// main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "unitTesting.h"
#include <string>
#include <sstream>
#include <iostream>
#include "vld.h"

using namespace std;

bool LinkedListTest()
{
	LinkedList<int> testList;
	int values[] = { 2, 4, 8, 3, 56, 34, 2, 4, 99, 0 };
	int valueCount = sizeof(values) / sizeof(values[0]);
	for (int i = 0; i < valueCount; i++)
	{
		testList.PushBack(values[i]);
	}
	for (int i = 0; i < valueCount; i++)
	{
		stringstream ss;
		ss << "Testing PushBack() at index " << i;
		string message = ss.str();
		if (!verify(values[i], testList.AtIndex(i)->value, message))
			return false;
	}

	if (!verify(valueCount, testList.Size(), "Size()"))
		return false;

	if (!verify(4, testList.Find(56), "Find()"))
		return false;

	testList.PopBack();
	if (!verify(-1, testList.Find(0), "PopBack()"))
		return false;

	testList.PopFront();
	if (!verify(4, testList.AtIndex(0)->value, "PopFront()"))
		return false;

	testList.Clear();
	if (!verify(0, testList.Size(), "Clear()"))
		return false;

	for (int i = 0; i < 20; i++)
	{
		testList.PushFront(i);
	}
	for (int i = 0; i < 20; i++)
	{
		stringstream ss;
		ss << "Testing PushFront() at index " << i;
		string message = ss.str();
		if (!verify(19 - i, testList.AtIndex(i)->value, message))
			return false;
	}

	testList.Clear();

	return true;
}

bool BinarySearchTreeTest()
{
	BinarySearchTree<int> testTree;

	int values[] = { 5, 3, 8, 45, 9 };
	int inOrder[] = { 3, 5, 8, 9, 45 };
	int postOrder[] = { 3, 9, 45, 8, 5 };
	int preOrder[] = { 5, 3, 8, 45, 9 };

	int valueCount = sizeof(values) / sizeof(values[0]);
	for (int i = 0; i < valueCount; i++)
	{
		testTree.Insert(values[i]);
	}

	LinkedList<int>* tempList = testTree.InOrder();
	
	for (int i = 0; i < valueCount; i++)
	{
		if (!verify(inOrder[i], tempList->AtIndex(i)->value, "InOrder()"))
			return false;
	}
	tempList->Clear();
	delete tempList;
	tempList = nullptr;

	tempList = testTree.PostOrder();
	for (int i = 0; i < valueCount; i++)
	{
		if (!verify(postOrder[i], tempList->AtIndex(i)->value, "PostOrder()"))
			return false;
	}
	tempList->Clear();
	delete tempList;
	tempList = nullptr;

	tempList = testTree.PreOrder();
	for (int i = 0; i < valueCount; i++)
	{
		if (!verify(preOrder[i], tempList->AtIndex(i)->value, "PreOrder()"))
			return false;
	}
	tempList->Clear();
	delete tempList;
	tempList = nullptr;

	if (!verify(5, testTree.Size(), "Size()"))
		return false;

	if (!verify(false, testTree.Find(12), "Find()"))
		return false;
	if (!verify(true, testTree.Find(8), "Find()"))
		return false;

	testTree.Clear();
	if (!verify(0, testTree.Size(), "Clear()"))
		return false;

	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (LinkedListTest())
		cout << endl << "Linked List OK!" << endl;
	else
		cout << endl << "Linked List FAIL!" << endl;

	cout << endl;

	if (BinarySearchTreeTest())
		cout << endl << "Binary Search Tree OK!" << endl;
	else
		cout << endl << "Binary Search Tree FAIL!" << endl;

	cout << endl;

	system("pause");
	return 0;
}